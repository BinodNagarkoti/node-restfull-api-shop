const mongoose= require('mongoose')
const Schema = mongoose.Schema;
const orderScheme =Schema({
    _id:mongoose.Schema.Types.ObjectId,
    product:{type:mongoose.Schema.Types.ObjectId,ref:'Product',require:true},
    quantity:{type:Number, default:1}
   
})
module.exports=mongoose.model('Order',orderScheme)