const OrdersControllers = require('../controllers/orders.js')
const express = require('express')
const router = express.Router();
const checkAuth = require('../middleware/check-auth')
//Handle incomming GET request in /orders
router.get('/', checkAuth, OrdersControllers.getAllOrders)
//Handle incomming POST request in /orders
router.post('/', checkAuth, OrdersControllers.postOrders)
//Handle incomming GET request in /orders/orderId

router.get('/:orderId', checkAuth, OrdersControllers.getOrdersById)
//Handle incomming DELETE request in /orders/orderId
router.delete('/:orderId', checkAuth, OrdersControllers.deleteOrdersById)
module.exports = router