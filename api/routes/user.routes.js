const express = require('express')
const router = express.Router();

const UserController = require('../controllers/user.js')
router.post('/signup', UserController.signup)
router.get('/', UserController.getAllUsers)
router.post('/login', UserController.login)


router.delete('/', UserController.deleteById)
module.exports = router;