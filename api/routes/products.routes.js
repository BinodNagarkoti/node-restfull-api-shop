const express = require('express')
const router = express.Router();

const checkAuth = require('../middleware/check-auth')
const ProductController = require('../controllers/products.js')
// get all products ans its details (get method)
router.get('/', ProductController.getAllPoducts)
//product insertion ( post method)
router.post('/', checkAuth, ProductController.postProduct)
//get product with id in url (get method)
router.get('/:productId', ProductController.getProductById)
//update product with id in url
router.patch('/:productId', checkAuth, ProductController.patchProductById)
//deleting product with id in url
router.delete('/:productId', checkAuth,ProductController.deleteProductById)
module.exports = router