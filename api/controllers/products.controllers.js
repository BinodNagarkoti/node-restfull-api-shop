const Product = require('../models/product.models')
const mongoose = require('mongoose')
exports.getAllPoducts = (req, res, next) => {
    Product.find().select("name price _id").exec().then(docs => {
        const response = {
            count: docs.length,
            products: docs.map(doc => {
                return {
                    messages: "Fetch Successfully ",
                    products: {
                        id: doc._id,
                        name: doc.name,
                        price: doc.price,
                        requests: {
                            type: "GET",
                            url: 'http://localhost:5000/products/' + doc._id
                        }
                    }
                }
            }),
        }
        console.log("Array of Products:", response)
        if (docs.length > 0) {
            res.status(200).json(response)
        } else {
            res.status.json({ message: "No Entries Found" })
        }
    }).catch(err => {
        res.status(200).json({
            error: err
        })
    })

}
exports.postProduct = (req, res, next) => {
    const product = new Product({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name,
        price: req.body.price
    })
    product.save().then(results => {
        console.log(results)
        res.status(200).json({
            createdProduct: {
                id: results._id,
                name: results.name,
                price: results.price,
                request: {
                    type: "GET",
                    url: 'http://localhost:5000/products/' + results._id
                }
            },
            message: "Product Object Created Successfully",
        })
    }).catch(err => {
        console.log(err)
        res.status(500).json({ error: err })
    }
    )
}
exports.getProductById = (req, res, next) => {
    const id = req.params.productId;
    Product.findById(id).select("name price _id").exec().then(docs => {
        console.log(docs)
        if (docs) {
            res.status(200).json({
                products: {
                    id: docs._id,
                    price: docs.price,
                    name: docs.name,
                    request: {
                        type: "GET",
                        url: 'http://localhost:5000/products/'
                    }
                }
            })
        } else {
            res.status(404).json({
                message: `No valid entry found for ProductId:${id}`
            })
        }
    }).catch(err => {
        console.log(err)
        res.status(500).json({
            error: err
        })
    })
}
exports.patchProductById = (req, res, next) => {
    const id = req.params.productId;
    const updateOps = {}
    //request body should be like: (for Dynamic Update)
    //eg: [ {"propName":"price","value":200 },{"propName":"name","value":"harry potter 6"} ]
    for (const ops of req.body) {
        updateOps[ops.propName] = ops.value
    }
    Product.update({ _id: id }, { $set: updateOps }).exec().then(result => {
        console.log("after updating:", result)
        res.status(200).json({
            message: "Product Updated",
            request: {
                type: "GET",
                url: 'http://localhost:5000/products/' + id
            }
        })
    }).catch(err => {
        console.log(err)
        res.status(500).json({ error: err })
    })
    res.status(200).json({
        message: "Updated Product"
    })
}
exports.deleteProductById = (req, res, next) => {
    const id = req.params.productId
    Product.remove({ _id: id }).exec().then(result => {
        res.status(200).json({
            message: "Product Deleted Successfully",
            request: {
                type: "POST",
                url: 'http://localhost:5000/products/',
                body: { name: 'String', price: 'Number' }
            }
        })
    }).catch(err => {
        console.log(err)
        res.status(500).json({ error: err })
    })
}