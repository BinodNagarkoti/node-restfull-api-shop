const User = require('../models/user.models')
const bcrypt = require('bcrypt')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
exports.signup = (req, res, next) => {
    User.find({ email: req.body.email }).exec().then(results => {
        if (!(results.length >= 1)) {
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                if (err) {
                    return res.status(500).json({
                        error: err,
                        message: "we couldn't save your password"
                    })
                } else {
                    const user = new User({
                        _id: new mongoose.Types.ObjectId(),
                        email: req.body.email,
                        password: hash
                    })
                    user.save().then(result => {
                        console.log(result)
                        res.status(201).json({
                            message: "User Created"
                        })
                    }).catch(err => {
                        console.log("error:", err)
                        res.status(500).json({
                            error: err
                        })
                    })
                }
            })
        } else {
            console.log("Email Already Existed", results)
            res.status(409).json({
                message: "Email Already Existed",
                suggestion: "Try another email"
            })
        }
    }).catch(err => {
        console.log(err)
        res.status(500).json({
            error: err
        })
    })
}
exports.getAllUsers = (req, res, next) => {
    User.find().exec().then(docs => {
        console.log(docs)
        if (docs.length >= 1) {
            res.status(200).json({
                count: docs.length,
                users: docs
            })
        } else {
            res.status(404).json({
                message: "there's no any users added",
                request: {
                    type: "POST",
                    url: `http://localhost:5000/user/signup`,
                    body: { "email": "String", "password": "String" }
                }
            })
        }
    }).catch(err => {
        res.status(500).json({
            error: err,

        })
    })
}
exports.login = (req, res, next) => {
    User.findOne({ email: req.body.email }).then(user => {
        console.log("users:", user)
        if (user) {
            console.log("KEY:", process.env.JWT_KEY)
            bcrypt.compare(req.body.password, user.password, (err, result) => {
                if (result) {
                    const token = jwt.sign(
                        { email: user.email, userId: user._id },
                        "secret",
                        {
                            expiresIn: "1h"
                        })
                    res.status(200).json({
                        message: "Login Successful",
                        token: token
                    })
                } else {
                    res.status(401).json({
                        error: "Password didn't match",
                        message: "Auth failed"
                    })
                } if (err) {
                    res.status(401).json({
                        error: err,
                        message: "Auth failed"
                    })
                }

            })

        } else {
            res.status(401).json({
                message: " Auth failed"
            })
        }
    }).catch(err => {
        console.log("error:", err)
        res.status(500).json({
            message: "email not found",
            error: err
        })
    })

}
exports.deleteById = (req, res, next) => {
    User.remove({ email: req.body.email }).then(result => {
        console.log(result)
        res.status(200).json({
            message: " User remove succesfully"
        })
    }).catch(err => {
        console.log("error:", err)
        res.status(500).json({

            message: "unable to delete user",
            error: err
        })
    })
}
