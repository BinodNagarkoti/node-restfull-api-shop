const mongoose = require('mongoose')


const Order = require('../models/order.models')
const Product = require('../models/product.models')
exports.getAllOrders = (req, res, next) => {
    Order.find().exec().then(docs => {
        console.log(docs)
        res.status(200).json({
            message: 'Orders were feteched Sucessfully',
            count: docs.length,
            orders: docs.map(doc => {
                return {
                    orderId: doc._id,
                    productId: doc.product,
                    quantity: doc.quantity,
                    orderRequest: {
                        type: "GET",
                        url: 'http://localhost:5000/' + doc._id
                    },
                    productRequest: {
                        type: 'GET',
                        url: 'http://localhost:5000/' + doc.product
                    }
                }
            })
        })
    })
}
exports.postOrders = (req, res, next) => {
    Product.findById(req.body.productId).then(product => {
        if (!product) {
            return res.status(404).json({
                message: " Product not Found"
            })
        }
        const order = new Order({
            _id: mongoose.Types.ObjectId(),
            product: product._id,
            quantity: req.body.quantity
        })
        return order.save()
    }

    ).then(results => {
        console.log(results)
        res.status(200).json({
            message: 'Order was created', results
        })
    }).catch(err => {
        console.log(error)
        res.status.json({
            error: err,
            message: "product not found"
        })
    })

}
exports.getOrdersById = (req, res, next) => {
    const id = req.params.orderId
    Order.findById(id).select("quantity product _id").exec().then(results => {
        console.log(results)
        res.status(200).json({
            order: results,
            request: {
                type: "GET",
                url: "http:localhost:5000/orders/",

            }
        }
        )
    }
    ).catch(err => {
        console.log(err)
        res.status(500).json({
            error: err
        })
    })

}
exports.deleteOrdersById = (req, res, next) => {
    Order.remove({ _id: req.params.orderId }).then(results => {
        if (!results) {
            res.status(404).json({
                message: "There's no order to Delete"
            })
        }

        res.status(200).json({
            message: "Order Deletion is Successfully Executed",
            request: {
                type: "POST",
                url: "http:localhost:5000/orders/",
                body: {
                    productId: "ID, Required",
                    quantity: "Number, Not Required ,Default_Value:1 "
                }
            }
        })
    }).catch(err => {
        res.status(500).json({
            error: err
        })
    })

}