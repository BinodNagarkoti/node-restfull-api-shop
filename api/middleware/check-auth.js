const jwt = require('jsonwebtoken')
module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1]
        console.log("token:", token)
        const secret = new Buffer(process.env.JWT_KEY, 'base64');
        const decoded = jwt.verify(token, "secret")
        req.userData = decoded
        console.log(decoded)
        next();

    } catch (error) {
        console.log(error)
        return res.status(401).json({
            error: error,
            message: 'Auth failed'
        })
    }
}