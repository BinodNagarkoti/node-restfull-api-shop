const express = require("express");
const app = express();
//importing route defined under api folder
const productRoutes = require("./api/routes/products");
const orderRoutes = require("./api/routes/orders");
const userRoutes = require("./api/routes/user");

const morgan = require("morgan");
const bodyParser = require("body-parser");
const mongoose = require('mongoose')
require("dotenv").config();
app.use(morgan("dev"));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin,X-Requested-With,Content-Type,Accept,Authorization"
    );
    if (req.method == 'OPTIONS') {
        res.header("Access-Control-Allow-Methods", "PUT,POST,PATCH,DELETE,GET")
        return res.status(200).json({})
    }
    next()
});
//Routes which should handle requests
app.use("/products", productRoutes);
app.use("/orders", orderRoutes);
app.use("/user", userRoutes);


// mongoose.connect(`mongodb+srv://admin:${process.env.MOGO_ATLAS_PW}@cluster-eqwhj.gcp.mongodb.net/test?retryWrites=true&w=majority`)

const uri = process.env.ATLAS_URI;
console.log("value uri", process.env.ATLAS_URI)
mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true, useCreateIndex: true });
// establishing mongoose connection
const connection = mongoose.connection;
connection.once("open", () => {
    console.log("MongoDb database connection establised sucessfully");
}).on('error', (error) => {
    console.log(error)
})
mongoose.Promise = global.Promise

app.use((req, res, next) => {
    const error = new Error(" Not Found");
    error.status = 404;
    next(error);
});
app.use((error, req, res, next) => {
    res.status(error.status || 500).json({
        error: {
            status: error.status || 500,
            message: error.message
        }
    });
});
module.exports = app;
